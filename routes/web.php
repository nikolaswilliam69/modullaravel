<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/book', 'BukuController@bookIndex');
Route::get('/createbook', 'BukuController@bookShow');
Route::post('/createbook', 'BukuController@bookCreate')->name('book.add');
Route::get('/book/{id}', 'BukuController@bookEdit')->name('book.edit');
Route::patch('/book/{id}', 'BukuController@bookUpdate')->name('book.update');
Route::delete('/book/{id}', 'BukuController@bookDelete')->name('book.delete');