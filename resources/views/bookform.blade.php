<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Add your Book</title>
    </head>
    <body>
        <form action="{{route('book.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <label for="title">Book Title:</label>
                <input type="text" id="title" placeholder="your book title" name="title">
                <label for="author">Author:</label>
                <input type="text" id="author" placeholder="your author" name="author">
                <label for="genre">Genre:</label>
                <input type="text" id="genre" placeholder="book genre" name="genre">
                <input type="submit" value="Add Book">
            </div>
        </form>
    </body>
</html>