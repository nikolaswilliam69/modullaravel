<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{asset('css/form.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta http-equiv="content-type" content="text/html; charset= UTF-8">
        <title>Edit your Book</title>
    </head>
    <body>
        <form action="{{route('book.update', $books->id)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
            @csrf
            <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $messages)
                    <li>{{$messages}}</li>
                @endforeach
                </ul>
                </div>
            @endif
                <label for="bookname">BookName:</label>
                <input type="text" id="bookname" placeholder="your bookname" value="{{$books->bookname}}" name="bookname">
                <label for="author">Author:</label>
                <input type="text" id="author" placeholder="Please enter author name" value="{{$books->author}}" name="author">
                <label for="genre">Genre:</label>
                <input type="text" id="genre" placeholder="Please enter the book genre" value="{{$books->genre}}" name="genre">
                <input type="submit" value="Edit Book">
            </div>
        </form>
    </body>
</html>