<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View The Books</title>
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Book Title</th>
                    <th>Author</th>
                    <th>Genre</th>
                </tr>
            </thead>
            <tbody>
                @foreach($books as $book)
                <tr>
                    <td>{{$book->bookname}}</td>
                    <td>{{$book->author}}</td>
                    <td>{{$book->genre}}</td>
                    <td><a href="{{route('book.edit', $book->id)}}" class="btn btn-success">Edit</a></td>
                    <td><form action="{{route('book.delete', $book->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button> 
                    </form></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>