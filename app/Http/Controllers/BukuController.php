<?php

namespace App\Http\Controllers;

use App\Buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookIndex()
    {
        $books = Buku::all();
        return view('book', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookCreate(Request $request)
    {
        $books = Buku::create([
            'bookname'=>$request->title,
            'author'=>$request->author,
            'genre'=>$request->genre
        ]);
        return redirect('/book');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function bookShow(Buku $buku)
    {
        return view('bookform');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function bookEdit(Buku $buku, $id)
    {
        $books=Buku::findOrfail($id);
        return view('editbooks', compact('books'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function bookUpdate(Request $request, Buku $buku, $id)
    {
        Buku::findOrfail($id)->update([
            'bookname'=>$request->bookname,
            'author'=>$request->author,
            'genre'=>$request->genre
        ]);
        return redirect('/book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function bookDelete(Buku $buku, $id)
    {
        Buku::destroy($id);
        return back();
    }
}
